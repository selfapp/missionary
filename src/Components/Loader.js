import React from 'react';
import { ActivityIndicator,
    StyleSheet,
    View,
    Dimensions
} from 'react-native';
import colors from '../styles/colors';
import { Colors } from 'react-native/Libraries/NewAppScreen';

export default Loader = (props) => {
    return (
        props.loading
            ?
            <View style={styles.loader}>
                <ActivityIndicator size="large" color={colors.appColor} />
            </View>
            :
            null
    )
}

const styles = StyleSheet.create({
    loader: {
        backgroundColor: 'rgba(245,245,245, 0.7)',
        height: Dimensions.get('window').height,
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignSelf: 'center',
        justifyContent: 'center'
    }
});
