import React from 'react';
import { Image } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator} from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import colors from '../styles/colors';
import Splash from '../Screens/Splash';
import LookMeUp from '../Screens/LookMeUp';
import SignIn from '../Screens/SignIn';
import ForgotPassword from '../Screens/ForgotPassword';
import SignUp from '../Screens/SignUp';


export const signUpNaviationOptions = createStackNavigator({
    LookMeUp:{ screen: LookMeUp },
    SignIn: { screen: SignIn },
    ForgotPassword: { screen: ForgotPassword},
    SignUp: { screen: SignUp }
},{
    headerMode: 'none'
})
// export const completeProfile = createStackNavigator({
    
// },{
//     headerMode: 'none'
// })

// export const profileNavigation = createStackNavigator({
   
// },{
//     headerMode: 'none'
// })

// export const bottomTabNavigator = createBottomTabNavigator(
//     {
//         Jobs:{
//             screen: Jobs,
//             navigationOptions:{
//                 tabBarIcon: ({tintColor}) =>{
//                     return(
//                         <Image
//                         source = {require('../assets/jobs.png')}
//                         style = {{tintColor:tintColor}}
//                         />
//                     )
//                 }
//             }
//         },
//         Messages:{
//             screen: Jobs,
//             navigationOptions:{
//                 tabBarIcon: ({tintColor}) =>{
//                     return(
//                         <Image
//                         source = {require('../assets/message.png')}
//                         style = {{tintColor:tintColor}}
//                         />
//                     )
//                 }
//             }
//         },
//         Profile:{
//           screen: profileNavigation,
//           navigationOptions:{
//               tabBarIcon: ({tintColor}) =>{
//                   return(
//                       <Image
//                       source = {require('../assets/user.png')}
//                       style = {{tintColor:tintColor}}
//                       />
//                   )
//               }
//           }
//       },
//     },
//     {
//         initialRouteName: 'Jobs',        
//         tabBarOptions: {
//             activeTintColor: colors.appColor,
//             inactiveTintColor: colors.gray,
//             showLabel: true,
//             showIcon: true,
//             tabBarPosition: 'bottom',
//             labelStyle: {
//                 fontSize: 12,
//             },
//             iconStyle:{
//                 width: 30,
//                 height: 30
//             },
//             style: {
//                 backgroundColor: 'rgb(245,245,245)',
//                 borderBottomWidth: 1,
//                 borderBottomColor: '#ededed',
//                 alignItems: 'center',
//                 justifyContent: 'center',
//                 alignSelf: 'center',
//             },
//             lazy: true,
//             indicatorStyle: '#fff',
//         }
//     }
//   );


export const appNavigationOptions = createStackNavigator({
    Splash: { screen: Splash},
     signUpNaviationOptions: { screen: signUpNaviationOptions},
    // completeProfile: { screen: completeProfile},
    // bottomTabNavigator: { screen: bottomTabNavigator}
},{
    headerMode: 'none'
})

export const AppContainer = createAppContainer(appNavigationOptions);