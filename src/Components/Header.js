import React from 'react';
import {
    View,
    TouchableOpacity,
    Image,
} from 'react-native';
import {BoldText, LightText} from '../Components/styledTexts';
import colors from '../styles/colors';

export default Header = (props) => {
    return (
        <View style={{flex:1, paddingVertical: 5 }}>
          <View style={{ flex:1, flexDirection:'row', alignItems:'flex-end', justifyContent:'flex-end', backgroundColor: props.backgroundColor ? props.backgroundColor : '#fff', backgroundColor: '#FFF',  shadowOffset:{width:0, height:2}, shadowOpacity: 0.18, shadowRadius:1, shadowColor: '#451B2D', elevation: 3 }}>
          <View style={{ paddingVertical:1, flex: 0.5, alignItems:'center', justifyContent:'center', }}>
            {props.leftNavigation ? (
            <TouchableOpacity 
              onPress={() => {
                  props.leftNavigation.goBack(null);
              }}
            >
              <Image style={{height: 40, width: 30, marginLeft: 5}}
                      resizeMode='contain'
                      source={require('../assets/bqck-arrow.png')}
              />
            </TouchableOpacity>
            ) : (null)}
 
            </View>
            <View style={{ flex: 2, alignItems:'center', justifyContent:'center', paddingVertical:10,  }}>
                <LightText style={{ fontSize: 18, color: colors.dimGray, textAlign:'center'}}>{props.value}</LightText>
           </View>
          <View style={{flex: 0.5}}>
          {props.rightIcon ? (
            <TouchableOpacity 
             onPress={()=> {
               props.onPressRight()}}
            >
              <Image style={{marginBottom: 7, height: 25, width: 25, marginLeft: 5}}
                      resizeMode='contain'
                      source={props.rightIcon}
              />
            </TouchableOpacity>
          ) : (null)}

          </View>
          </View>
          {/* <View style={{ height: .9, borderRadius: 1, backgroundColor: '#FFF',  shadowOffset:{width:0, height:2}, shadowOpacity: 0.18, shadowRadius:1, shadowColor: '#451B2D', elevation: 3 }}/> */}

          {/* <View style={{  height: .9,  backgroundColor:colors.lightGray}}/> */}

        </View>
    )
}