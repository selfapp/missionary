import React, {Component} from 'react';
import {
    View,
    Image
} from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';

export default class Splash extends Component{
    constructor(props){
        super(props);
    }
    componentDidMount(){
        
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'signUpNaviationOptions' })],
          });
          
          this.props.navigation.dispatch(resetAction);
    }

    render(){
        return(
                <View style={{ flex: 1, alignItems:'center', justifyContent:'center'}}>
                 <View style={{ flex:1, alignItems:'center', justifyContent:'center'}}>
                    <Image
                        source={require('../assets/globe.png')}
                    />
                </View>
               </View>
        )
    }
}