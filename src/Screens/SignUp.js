import React, { Component } from 'react';
import {
    View,
    ScrollView,
    TouchableOpacity,
    Image,
    SafeAreaView
} from 'react-native';
import Header from '../Components/Header';
import { Button } from '../Components/button';
import { BoldText, TextInputField, LightText } from '../Components/styledTexts';
import Colors from '../styles/colors';


export default class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isPrivacy: false,
            isTerms: false,
            isMissionary: false
        }
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: .12 }}>
                    <Header
                        leftNavigation={this.props.navigation}
                        value={'Join Our Adventure'}
                    />
                </View>
                <View style={{ flex: 1, paddingHorizontal: 20 }}>
                    <SafeAreaView style={{ flex: 1}}>
                    <ScrollView style={{ flex: 1}} centerContent={true} indicatorStyle={'white'} showsVerticalScrollIndicator={false}>
                    <View style={{ marginVertical: 20, alignItems: 'center', justifyContent: 'center' }}>
                        <BoldText style={{ fontSize: 26, color: Colors.embarkServe, fontWeight: '700' }}>Embark Serve</BoldText>
                    </View>
                    <View style={{ flex: 1 }}>
                        <View style={{ justifyContent: 'center', marginVertical: 10 }}>
                            <BoldText style={{ marginVertical: 5, color: Colors.charcoal }}>First Name</BoldText>
                            <TextInputField
                                containerStyle={{ borderColor: 'gray', borderWidth: 1, paddingHorizontal: 10 }}
                                placeholder={'Enter First Name'}
                            />
                        </View>
                        <View style={{ justifyContent: 'center', marginVertical: 10 }}>
                            <BoldText style={{ marginVertical: 5, color: Colors.charcoal }}>Last Name</BoldText>
                            <TextInputField
                                containerStyle={{ borderColor: 'gray', borderWidth: 1, paddingHorizontal: 10 }}
                                placeholder={'Enter Last Name'}
                            />
                        </View>
                        <View style={{ height: 40, flexDirection:'row', alignItems:'center', justifyContent: 'space-between'}}>
                            <LightText>Are you a soon to be or current missionary?</LightText>
                            <TouchableOpacity
                                onPress={()=>this.setState({ isMissionary: !this.state.isMissionary})}
                            >
                                <Image
                                    source={(this.state.isMissionary) ? require('../assets/check.png') : require('../assets/without-check.png')}
                                    style={{ height: 25, width: 25}}
                                />
                            </TouchableOpacity>
                        </View>
                        <View style={{ justifyContent: 'center',marginVertical: 10 }}>
                            <View style={{ flexDirection: 'row' , alignItems:'center', justifyContent: 'space-between'}}>
                                <BoldText style={{ marginVertical: 5, color: Colors.charcoal }}>Call</BoldText>
                                <LightText>Prefferred Method?</LightText>
                            </View>
                            <TextInputField
                                containerStyle={{ borderColor: 'gray', borderWidth: 1, paddingHorizontal: 10 }}
                                placeholder={'Enter Phone'}
                            />
                        </View>
                        <View style={{ justifyContent: 'center', marginVertical: 10 }}>
                            <BoldText style={{ marginVertical: 5, color: Colors.charcoal }}>Email</BoldText>
                            <TextInputField
                                containerStyle={{ borderColor: 'gray', borderWidth: 1, paddingHorizontal: 10 }}
                                placeholder={'Enter Email'}
                            />
                        </View>
                        <View style={{ justifyContent: 'center', marginVertical: 10 }}>
                            <BoldText style={{ marginVertical: 5, color: Colors.charcoal }}>Password</BoldText>
                            <TextInputField
                                containerStyle={{ borderColor: 'gray', borderWidth: 1, paddingHorizontal: 10 }}
                                placeholder={'Enter Password'}
                            />
                        </View>
                        <View style={{ height: 40, flexDirection:'row', alignItems:'center', justifyContent: 'space-between'}}>
                            <LightText>Agree to Terms & Condition</LightText>
                            <TouchableOpacity
                                onPress={()=>this.setState({ isTerms: !this.state.isTerms})}
                            >
                                <Image
                                    source={(this.state.isTerms) ? require('../assets/check.png') : require('../assets/without-check.png')}
                                    style={{ height: 25, width: 25}}
                                />
                            </TouchableOpacity>
                        </View>
                        <View style={{ height: 40, flexDirection:'row', alignItems:'center', justifyContent: 'space-between'}}>
                            <LightText>Agree to Privacy Policy</LightText>
                            <TouchableOpacity
                                onPress={()=>this.setState({ isPrivacy: !this.state.isPrivacy})}
                            >
                                <Image
                                    source={(this.state.isPrivacy) ? require('../assets/check.png') : require('../assets/without-check.png')}
                                    style={{ height: 25, width: 25}}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{  paddingVertical: 20, alignItems: 'center' }}>
                        <Button
                            value={'Embark!'}
                            color={Colors.button}
                            style={{ paddingHorizontal: 15, width: '90%' }}
                        />
                    </View>
                    </ScrollView>
                    </SafeAreaView>
                </View>
            </View>
        )
    }
}