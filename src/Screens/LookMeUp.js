import React, { Component } from 'react';
import {
    View,
    Image,
    TextInput,
    Share
} from 'react-native';
import Header from '../Components/Header';
import { BoldText, LightText } from '../Components/styledTexts';
import Colors from '../styles/colors';
import { Button } from '../Components/button';

export default class LookMeUp extends Component {
    constructor(props) {
        super(props);
    }
    onShare = async () => {
        try {
          const result = await Share.share({
            message:
              'You must be so proud of yourself, share Missinory with your friends to give them happiness',
          });
    
          if (result.action === Share.sharedAction) {
            if (result.activityType) {
              // shared with activity type of result.activityType
            } else {
              // shared
            }
          } else if (result.action === Share.dismissedAction) {
            // dismissed
          }
        } catch (error) {
          alert(error.message);
        }
      };

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: .12 }}>
                    <Header
                        value={'Connect: Guess my mission: Support'}
                    />
                </View>
                <View style={{ flex: 1 }}>
                    <View style={{ flex: 1, padding: 20 }}>
                        <View style={{ flex: 2, alignItems: 'center' }}>
                            <BoldText style={{ fontSize: 26, color: Colors.embarkServe, fontWeight: '700' }}>Embark Serve</BoldText>
                            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'space-around' }}>
                                <Image
                                    source={require('../assets/male.png')}
                                />
                                
                                <Image
                                    source={require('../assets/globe.png')}
                                />
                                <Image
                                    source={require('../assets/female.png')}
                                />
                                <LightText>Look me Up</LightText>
                            </View>
                        </View>
                        <View style={{ flex: 1, justifyContent: 'space-around' }}>
                            <View style={{ flex: 1 }}>
                                <View style={{ height: 40, backgroundColor: '#fff', marginHorizontal: 10, alignItems: 'center', justifyContent: 'center', marginTop: 10, borderRadius: 25, backgroundColor: '#FFFFFF', shadowColor: '#451B2D', shadowOffset: { width: 0, height: 3 }, shadowOpacity: 0.15, shadowRadius: 21, elevation: 3 }}>
                                    <View style={{ paddingLeft: 10, height: 40, flexDirection: 'row', alignItems: 'center', marginHorizontal: 20 }}>
                                        <Image
                                            source={require('../assets/search-icon.png')}
                                            style={{ height: 20, width: 20, tintColor: 'gray' }}
                                        />
                                        <TextInput
                                            style={{ flex: 1, paddingLeft: 5 }}
                                            placeholder={'Search'}
                                        />
                                    </View>
                                </View>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-around' }}>
                                <Button
                                    value={'Share App'}
                                    color={Colors.button}
                                    style={{ paddingHorizontal: 15 }}
                                    onPress={()=> this.onShare()}
                                />
                                <Button
                                    value={'Join'}
                                    color={Colors.button}
                                    style={{ paddingHorizontal: 25 }}
                                    onPress={()=>this.props.navigation.navigate('SignUp')}
                                />
                                <Button
                                    value={'Sign in'}
                                    color={Colors.button}
                                    style={{ paddingHorizontal: 20 }}
                                    onPress={()=>this.props.navigation.navigate('SignIn')}
                                />
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}