import React, { Component } from 'react';
import {
    View,
    TouchableOpacity
} from 'react-native';
import Header from '../Components/Header';
import { Button } from '../Components/button';
import { BoldText, TextInputField, LightText } from '../Components/styledTexts';
import Colors from '../styles/colors';


export default class SignIn extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: .12 }}>
                    <Header
                        leftNavigation={this.props.navigation}
                        value={'Sign - In'}
                    />
                </View>
                <View style={{ flex: 1, paddingHorizontal: 20 }}>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <BoldText style={{ fontSize: 26, color: Colors.embarkServe, fontWeight: '700' }}>Embark Serve</BoldText>
                    </View>
                    <View>
                        <View style={{ justifyContent: 'center' }}>
                        
                            <BoldText style={{ marginVertical: 5, color: Colors.charcoal }}>Email</BoldText>
                            <TextInputField
                                containerStyle={{ borderColor: 'gray', borderWidth: 1, paddingHorizontal: 10 }}
                                placeholder={'Enter Email'}
                            />
                        </View>
                        <View style={{ justifyContent: 'center', marginVertical: 10 }}>
                            <BoldText style={{ marginVertical: 5, color: Colors.charcoal }}>Password</BoldText>
                            <TextInputField
                                containerStyle={{ borderColor: 'gray', borderWidth: 1, paddingHorizontal: 10 }}
                                placeholder={'Enter Password'}
                                secureTextEntry={true}
                            />
                           
                        </View> 
                        <View style={{ marginVertical: 5, justifyContent: 'flex-end', alignItems:'flex-end'}}>
                            <TouchableOpacity
                                onPress={() => this.props.navigation.navigate('ForgotPassword')}
                            >
                                <LightText> Forgot password?</LightText>
                            </TouchableOpacity>
                            </View>
                    </View>
                    <View style={{ flex: 2, paddingVertical: 20, alignItems: 'center', justifyContent: 'space-evenly' }}>
                        <Button
                            value={'Embark!'}
                            color={Colors.button}
                            style={{ paddingHorizontal: 15, width: '90%' }}
                        />
                        <BoldText>OR</BoldText>
                        <LightText style={{ textAlign: 'center', paddingHorizontal: 20 }}>Join us to claim your guesses and support you missionary:</LightText>
                        <Button
                            value={'Join the Fun'}
                            color={Colors.button}
                            style={{ paddingHorizontal: 15, width: '90%' }}
                            onPress={()=>this.props.navigation.navigate('SignUp')}
                        />
                    </View>
                </View>
            </View>
        )
    }
}