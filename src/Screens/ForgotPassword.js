import React, { Component } from 'react';
import {
    View,
    TouchableOpacity,
    Image
} from 'react-native';
import Header from '../Components/Header';
import { Button } from '../Components/button';
import { BoldText, TextInputField } from '../Components/styledTexts';
import Colors from '../styles/colors'


export default class ForgotPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isCell: false,
            isEmail: false
        }
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: .12 }}>
                    <Header
                        leftNavigation={this.props.navigation}
                        value={'Forgot Pasword'}
                    />
                </View>
                <View style={{ flex: 1, paddingHorizontal: 20 }}>
                    <View style={{ marginVertical: 20, alignItems: 'center', justifyContent: 'center' }}>
                        <BoldText style={{ fontSize: 26, color: Colors.embarkServe, fontWeight: '700' }}>Embark Serve</BoldText>
                    </View>
                    <View style={{ justifyContent: 'center' }}>

                        <BoldText style={{ marginVertical: 5, color: Colors.charcoal }}>Cell</BoldText>
                        <TextInputField
                            containerStyle={{ borderColor: Colors.lightGray, borderWidth: 1, paddingHorizontal: 10 }}
                            placeholder={'Cell'}
                        />
                        <View style={{ marginTop: 10, height: 40, alignItems: 'flex-end', justifyContent: 'space-between' }}>
                            <TouchableOpacity
                                onPress={() => this.setState({ isCell: !this.state.isCell })}
                            >
                                <Image
                                    source={(this.state.isCell) ? require('../assets/check.png') : require('../assets/without-check.png')}
                                    style={{ height: 20, width: 20 }}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{ justifyContent: 'center' }}>

                        <BoldText style={{ color: Colors.charcoal }}>Email</BoldText>
                        <TextInputField
                            containerStyle={{ borderColor: Colors.lightGray, borderWidth: 1, paddingHorizontal: 10 }}
                            placeholder={'Enter Email'}
                        />
                        <View style={{ marginVertical: 10, height: 40, alignItems: 'flex-end', justifyContent: 'space-between' }}>
                            <TouchableOpacity
                                onPress={() => this.setState({ isEmail: !this.state.isEmail })}
                            >
                                <Image
                                    source={(this.state.isEmail) ? require('../assets/check.png') : require('../assets/without-check.png')}
                                    style={{ height: 20, width: 20 }}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <Button
                            value={'Embark!'}
                            color={Colors.button}
                            style={{ paddingHorizontal: 15, width: '90%' }}
                        />
                </View>
            </View>
        )
    }
}